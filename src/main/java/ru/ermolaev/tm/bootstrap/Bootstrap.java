package ru.ermolaev.tm.bootstrap;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.api.endpoint.*;
import ru.ermolaev.tm.api.repository.*;
import ru.ermolaev.tm.api.service.*;
import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.endpoint.*;
import ru.ermolaev.tm.enumeration.Role;
import ru.ermolaev.tm.exception.unknown.UnknownArgumentException;
import ru.ermolaev.tm.exception.unknown.UnknownCommandException;
import ru.ermolaev.tm.exception.empty.EmptyCommandException;
import ru.ermolaev.tm.repository.*;
import ru.ermolaev.tm.service.*;
import ru.ermolaev.tm.util.TerminalUtil;

import javax.xml.ws.Endpoint;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public final class Bootstrap implements ServiceLocator {

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IUserService userService = new UserService(userRepository);

    @NotNull
    private final IAuthenticationService authenticationService = new AuthenticationService(userService);

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final IDomainService domainService = new DomainService(taskService, projectService, userService);

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @NotNull
    private final ISessionService sessionService = new SessionService(sessionRepository, this);

    @NotNull
    private final LoadService loadService = new LoadService(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final ISessionEndpoint sessionEndpoint = new SessionEndpoint(this);

    @NotNull
    private final IAdminUserEndpoint adminUserEndpoint = new AdminUserEndpoint(this);

    @NotNull
    private final IAdminDataEndpoint adminDataEndpoint = new AdminDataEndpoint(this);

    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @NotNull
    private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();

    @NotNull
    private final String url = "http://" + propertyService.getServerHost() + ":" + propertyService.getServerPort();

    @NotNull
    private final String sessionEndpointUrl = url + "/SessionEndpoint?wsdl";

    @NotNull
    private final String taskEndpointUrl = url + "/TaskEndpoint?wsdl";

    @NotNull
    private final String projectEndpointUrl = url + "/ProjectEndpoint?wsdl";

    @NotNull
    private final String userEndpointUrl = url + "/UserEndpoint?wsdl";

    @NotNull
    private final String adminUserEndpointUrl = url + "/AdminEndpoint?wsdl";

    @NotNull
    private final String adminDataEndpointUrl = url + "/AdminDataEndpoint?wsdl";

    private void initEndpoint() {
        Endpoint.publish(sessionEndpointUrl, sessionEndpoint);
        Endpoint.publish(taskEndpointUrl, taskEndpoint);
        Endpoint.publish(projectEndpointUrl, projectEndpoint);
        Endpoint.publish(userEndpointUrl, userEndpoint);
        Endpoint.publish(adminUserEndpointUrl, adminUserEndpoint);
        Endpoint.publish(adminDataEndpointUrl, adminDataEndpoint);
    }

    private void printEndpoint() {
        System.out.println(sessionEndpointUrl);
        System.out.println(taskEndpointUrl);
        System.out.println(projectEndpointUrl);
        System.out.println(userEndpointUrl);
        System.out.println(adminUserEndpointUrl);
        System.out.println(adminDataEndpointUrl);
    }

    private void initUsers() throws Exception {
        userService.create("user", "user", "user@test.ru");
        userService.create("test", "test", "test@test.ru");
        userService.create("guest", "guest", "guest@test.ru");
        userService.create("admin", "admin", Role.ADMIN);
        userService.create("root", "root", Role.ADMIN);
    }

    private void initProperty() {
        propertyService.init();
    }

    private void initCommands(@NotNull final List<AbstractCommand> commandList) {
        for (@NotNull AbstractCommand command: commandList) init(command);
    }

    private void init(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commands.put(command.commandName(), command);
        arguments.put(command.arg(), command);
    }

    @SneakyThrows
    public void run(@Nullable final String[] args) {
        System.out.println("Welcome to task manager");
        initCommands(commandService.getCommandList());
        initUsers();
        initEndpoint();
        printEndpoint();
        if (parseArgs(args)) System.exit(0);
        while (true) {
            try {
                parseCommand(TerminalUtil.nextLine());
            } catch (Exception e) {
                System.err.println(e.getMessage());
                System.err.println("[FAIL]");
            }
        }
    }

    public boolean parseArgs(@Nullable final String[] args) {
        if (args == null || args.length == 0) return false;
        @Nullable final String arg = args[0];
        if (arg == null) return false;
        try {
            parseArg(arg);
        } catch (Exception e) {
            System.err.println(e.getMessage());
            System.err.println("[FAIL]");
        }
        return true;
    }

    public void parseArg(@NotNull final String arg) throws Exception {
        if (arg.isEmpty()) return;
        @Nullable final AbstractCommand argument = arguments.get(arg);
        if (argument == null) throw new UnknownArgumentException(arg);
        argument.execute();
    }

    public void parseCommand(@Nullable final String cmd) throws Exception {
        if (cmd == null || cmd.isEmpty()) throw new EmptyCommandException();
        @Nullable final AbstractCommand command = commands.get(cmd);
        if (command == null) throw new UnknownCommandException(cmd);
        authenticationService.checkRole(command.roles());
        command.execute();
    }

    @NotNull
    @Override
    public IUserService getUserService() {
        return userService;
    }

    @NotNull
    @Override
    public IAuthenticationService getAuthenticationService() {
        return authenticationService;
    }

    @NotNull
    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @NotNull
    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @NotNull
    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @NotNull
    @Override
    public IDomainService getDomainService() {
        return domainService;
    }

    @NotNull
    @Override
    public IPropertyService getPropertyService() {
        return propertyService;
    }

    @NotNull
    @Override
    public ISessionService getSessionService() {
        return sessionService;
    }

    @NotNull
    @Override
    public LoadService getLoadService() {
        return loadService;
    }

}
