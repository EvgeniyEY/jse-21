package ru.ermolaev.tm.command.data.binary;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.enumeration.Role;

public final class DataBinaryClearCommand extends AbstractCommand {

    @NotNull
    @Override
    public String commandName() {
        return "data-bin-clear";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove binary file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[REMOVE BINARY FILE]");
        serviceLocator.getLoadService().clearBinaryFile();
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }

}
