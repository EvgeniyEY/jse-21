package ru.ermolaev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.dto.Result;
import ru.ermolaev.tm.entity.Session;

import javax.jws.WebParam;

public interface ISessionEndpoint {

    @Nullable
    Session openSession(@Nullable String login, @Nullable String password) throws Exception;

    @NotNull
    Result closeSession(@Nullable Session session) throws Exception;

    @NotNull
    Result closeAllSessionsForUser(@Nullable Session session) throws Exception;

}
