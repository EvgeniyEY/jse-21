package ru.ermolaev.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.entity.Session;
import ru.ermolaev.tm.entity.User;

public interface IUserEndpoint {

    @Nullable
    User updateUserPassword(@Nullable Session session, @Nullable String newPassword) throws Exception;

    @Nullable
    User updateUserFirstName(@Nullable Session session, @Nullable String newFirstName) throws Exception;

    @Nullable
    User updateUserMiddleName(@Nullable Session session, @Nullable String newMiddleName) throws Exception;

    @Nullable
    User updateUserLastName(@Nullable Session session, @Nullable String newLastName) throws Exception;

    @Nullable
    User updateUserEmail(@Nullable Session session, @Nullable String newEmail) throws Exception;

    @Nullable
    User showUserProfile(@Nullable Session session) throws Exception;

}
