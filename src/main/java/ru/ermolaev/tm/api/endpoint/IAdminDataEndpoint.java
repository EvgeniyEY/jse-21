package ru.ermolaev.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.entity.Session;

public interface IAdminDataEndpoint {

    void saveXmlByJaxb(@Nullable Session session) throws Exception;

    void loadXmlByJaxb(@Nullable Session session) throws Exception;

    void clearXmlFileJaxb(@Nullable Session session) throws Exception;

    void saveXmlByFasterXml(@Nullable Session session) throws Exception;

    void loadXmlByFasterXml(@Nullable Session session) throws Exception;

    void clearXmlFileFasterXml(@Nullable Session session) throws Exception;

    void saveJsonByJaxb(@Nullable Session session) throws Exception;

    void loadJsonByJaxb(@Nullable Session session) throws Exception;

    void clearJsonFileJaxb(@Nullable Session session) throws Exception;

    void saveJsonByFasterXml(@Nullable Session session) throws Exception;

    void loadJsonByFasterXml(@Nullable Session session) throws Exception;

    void clearJsonFileFasterXml(@Nullable Session session) throws Exception;

    void saveBinary(@Nullable Session session) throws Exception;

    void loadBinary(@Nullable Session session) throws Exception;

    void clearBinaryFile(@Nullable Session session) throws Exception;

    void saveBase64(@Nullable Session session) throws Exception;

    void loadBase64(@Nullable Session session) throws Exception;

    void clearBase64File(@Nullable Session session) throws Exception;

}
